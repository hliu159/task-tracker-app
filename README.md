# Task Tracker App - React  
  
Functions  
•	Add a task by inputting task content, setting a date/time, setting a reminder  
•	View details of the task  
•	Delete task  
•	Double-click the existing task to set a reminder  
  
H Liu  
January 2023  
