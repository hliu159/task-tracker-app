import { useState } from 'react'
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'
import Header from "./components/Header";
import Tasks from "./components/Tasks";
import About from './components/About';
import TaskDetails from './components/TaskDetails';

function App() {
  
  const [showAddTask, setShowAddTask] = useState(false)
  const [tasks, setTasks] = useState([
    {
      "id": 1,
      "text": "Doctors Appointment",
      "day": "Feb 5th at 2:30pm",
      "reminder": true
    },
    {
      "id": 2,
      "text": "Meeting at School",
      "day": "Feb 6th at 1:30pm",
      "reminder": true
    }
  ])

  return (
    <Router>
    <div className="container">
      <Header setShowAddTask={() => setShowAddTask (!showAddTask)} showAddTask={showAddTask}/>
      <Routes>
        <Route path='/' element={<Tasks showAddTask={showAddTask} tasks={tasks} setTasks={setTasks}/> }/>
        <Route path="/about" element={<About />} />
        <Route path="/task/:id" element={<TaskDetails tasks={tasks}/>} />
      </Routes>
    </div>
    </Router>
  );

}

export default App;
