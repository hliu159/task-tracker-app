const Button = ({ color, text, setShowAddTask}) => {
  return (
    <button
      style={{ backgroundColor: color }}
      className='btn'
      onClick={setShowAddTask}
    >
      {text}
    </button>
  )
}

export default Button