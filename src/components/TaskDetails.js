const TaskDetails = ({tasks}) => {
  const url = window.location.href;
  const currentId = url.split('/')[4]
  const task = tasks.filter((task) => task.id == currentId)

  return (
    <>
      <h3>{task[0].text}</h3>
      <p>{task[0].day}</p>
      <a href="../" className="btn">Go Back</a>
    </>
  )
}

export default TaskDetails