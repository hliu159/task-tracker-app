import Button from './Button'
import { useLocation } from 'react-router-dom'

const Header = ({ title , setShowAddTask, showAddTask }) => {
  return (
    <header className='header'>
      <h1>{title}</h1>
      {useLocation().pathname === '/' && <Button color={showAddTask ? 'red' : 'green'} 
      text={showAddTask ? 'Close' : 'Add'}  setShowAddTask={setShowAddTask}/>}
    </header>
  )
}

Header.defaultProps = {
  title: 'Task Tracker',
}

export default Header
