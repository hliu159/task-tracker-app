import Task from './Task'
import AddTask from './AddTask'
import Footer from './Footer'

const Tasks = ({showAddTask, tasks, setTasks }) => {


  const deleteTask = (id) => {
    setTasks(tasks.filter((task) => task.id !== id
    ))
  }

  const toggleReminder = (id) => {
    setTasks(
      tasks.map((task) =>
        task.id === id ? { ...task, reminder: !task.reminder } : task
      )
    )
  }

  return (
    <>
    {showAddTask && <AddTask tasks={tasks} setTasks={setTasks}/>}
        {tasks.length>0 ? tasks.map((task) => (
          <Task key={task.id} task={task} onDelete={deleteTask} onToggle={toggleReminder}/>
        )) : <p>Nothing to show</p>
      }
    <Footer />
    </>
  )

}

export default Tasks